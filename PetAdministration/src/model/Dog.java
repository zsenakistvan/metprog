/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import superclasses.Animal;
import technicalFunctions.GetterFunctionName;

/**
 *
 * @author zsenakistvan
 */
public class Dog extends Animal{
    
    public static String species = "Wolf";
    
    @GetterFunctionName(value = "isWild")
    private Boolean wild;
    //private static String getterFunctionNameFoWild = "isWild";
    
    public Dog(String name, Integer age, Boolean male, Boolean wild){
        super(name, age, male);
        this.wild = wild;
    }
    
    public Boolean isWild(){
        return this.wild;
    }
    
    public void goWild(){
        this.wild = true;
    }
    
    public void goNonWild(){
        this.wild = false;
    }
    
    @Override
    public void voice(){
        System.out.println(this.name + ": Wooff");
    }
}
