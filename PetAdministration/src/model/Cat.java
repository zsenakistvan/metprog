/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Random;
import superclasses.Animal;

/**
 *
 * @author zsenakistvan
 */
public class Cat extends Animal{
    
    private BloodTypeEnum bloodType;

    private Cat() {
        
    }

    public Cat(String name, Integer age, Boolean male, BloodTypeEnum bloodType) {
        super(name, age, male);
        
        this.bloodType = bloodType;
        if(this.bloodType == null){
            this.bloodType = BloodTypeEnum.A;
        }
    }

    
    
    @Override
    public void voice(){
        System.out.println(this.name + ": Meow");
    }
    
    public Cat doSex(Cat partner){
        if(this.male != partner.male){
            Cat newCat = new Cat();
            newCat.name = "New Cat";
            newCat.age = 0;
            newCat.male = new Random().nextBoolean();
            if(this.bloodType.equals(partner.bloodType)){//==
                newCat.bloodType = this.bloodType;
            }
            else if(!this.bloodType.equals(partner.bloodType) && !this.bloodType.equals(BloodTypeEnum.AB) && !partner.bloodType.equals(BloodTypeEnum.AB)){//!=
                newCat.bloodType = BloodTypeEnum.AB;
            }
            else if(this.bloodType.equals(BloodTypeEnum.AB)){
                newCat.bloodType = partner.bloodType;
            }
            else{
                newCat.bloodType = this.bloodType;
            }
            return newCat;
        }
        return null;
    }
    
    public static Cat doSex(Cat oneCat, Cat otherCat){
        if(oneCat.male != otherCat.male){
            Cat newCat = new Cat();
            newCat.name = "New Cat";
            newCat.age = 0;
            newCat.male = new Random().nextBoolean();
            if(oneCat.bloodType.equals(otherCat.bloodType)){//==
                newCat.bloodType = oneCat.bloodType;
            }
            else if(!oneCat.bloodType.equals(otherCat.bloodType) && !oneCat.bloodType.equals(BloodTypeEnum.AB) && !otherCat.bloodType.equals(BloodTypeEnum.AB)){//!=
                newCat.bloodType = BloodTypeEnum.AB;
            }
            else if(oneCat.bloodType.equals(BloodTypeEnum.AB)){
                newCat.bloodType = otherCat.bloodType;
            }
            else{
                newCat.bloodType = oneCat.bloodType;
            }
            return newCat;
        }
        return null;
    }
    
    
    
    public Boolean checkBloodType(BloodTypeEnum indicator){
        if(indicator == this.bloodType){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    
    
}
