/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package technicalFunctions;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 *
 * @author zsenakistvan
 */
public class Functions <T>{
    
    public void writeOut(T entity){
        Class superclazz = entity.getClass().getSuperclass();
        Class clazz = entity.getClass();
        
        Field[] fieldsOfSuperclass = superclazz.getDeclaredFields();
        for(Field field : fieldsOfSuperclass){
            String gfn = field.getAnnotation(GetterFunctionName.class).value();
            try{
                Method getterFunction = superclazz.getMethod(gfn);
                String valueOfField = getterFunction.invoke(entity).toString();
                System.out.println(field.getName() + ": " + valueOfField);
            }
            catch(Exception ex){}
        }
        
    }
    
}
