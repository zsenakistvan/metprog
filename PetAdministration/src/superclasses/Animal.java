/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package superclasses;

import technicalFunctions.GetterFunctionName;

/**
 *
 * @author zsenakistvan
 */
public abstract class Animal {
    @GetterFunctionName(value = "getName")
    protected String name;
    @GetterFunctionName(value = "getAge")
    protected Integer age;
    @GetterFunctionName(value = "isMale")
    protected Boolean male;

    public Animal(String name, Integer age, Boolean male) {
        this.setName(name);
        this.age = age;
        if(this.age == null || this.age < 0){
            this.age = 0;
        }
        this.male = male;
        if(this.male == null) {
            this.male = false;
        }
    }
    
    public Animal(){}
    
    public String getName() {
        return this.name;
    }

    public Integer getAge() {
        return this.age;
    }

    public Boolean isMale() {
        return this.male;
    }

    public void setName(String name) {
        if(name != null){
            this.name = name;
        }
    }

    public void birthday() {
        this.age++;
    }
    
    public void voice(){
        System.out.println(this.name + ": ...");
    }
    
}
