/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package petadministration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import model.BloodTypeEnum;
import model.Cat;
import model.Dog;
import superclasses.Animal;
import technicalFunctions.Functions;


/**
 *
 * @author zsenakistvan
 */
public class PetAdministration {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Cat frisky = new Cat();
        //frisky.age++;
        
//        Integer x = 1 + 3;
//        String text = "apple" + 1;
//        
//        System.out.println(x);
//        System.out.println(text);
        
        Cat frisky = new Cat("Frisky", -4, Boolean.FALSE, BloodTypeEnum.AB);
        //frisky.age = 1;
        System.out.println(frisky.getName() + " is " + frisky.getAge() + " years old.");
        frisky.birthday();
        //frisky.setName("Bodri");
        System.out.println(frisky.getName() + " is " + frisky.getAge() + " years old.");
        frisky.voice();
        
        Cat bodri = new Cat("Bodri", 2, Boolean.TRUE, BloodTypeEnum.A);
        System.out.println(bodri.getName());
        Cat littleBodri = bodri.doSex(frisky);
        System.out.println(littleBodri.getName() + " " + littleBodri.getAge() + " " + littleBodri.isMale());
        System.out.println(littleBodri.checkBloodType(BloodTypeEnum.B));
    
        
        //Cat c = new Cat(); 
        //c.createCat("Saynyi", 4, true, BT.A);
        // Cat x = c.createCat("Saynyi", 4, true, BT.A);
        // Cat x = Cat.createCat("Saynyi", 4, true, BT.A);
        //Lis<Cat> cats = Cat.getAllCatsFromDb();
        //Cat y = new Cat=();
        //Lis<Cat> cats = y.getAllCatsFromDb();
        
        
        Cat newCat = Cat.doSex(bodri, frisky);
        bodri.voice();
    
        List<Cat> cats =  new ArrayList<Cat>();
        cats.add(littleBodri);
        cats.add(bodri);
        cats.add(frisky);
        System.out.println(Arrays.toString(cats.toArray()));
        for(Integer i = 0; i < cats.size(); i++){
            if(cats.get(i).getName().equals("Bodri")){
                cats.remove(i);
            }
        }
        System.out.println(Arrays.toString(cats.toArray()));
        
        
        frisky.voice();
        Dog scooby = new Dog("Scooby", -3, true, false);
        scooby.voice();
        
        //scooby.setWild(true);
        scooby.goWild();
        
        //Animal anim = new Animal("I do not know", -2, null);
        
        
        Functions<Dog> f = new Functions<>();
        f.writeOut(scooby);
        Functions<Cat> ff = new Functions<>();
        ff.writeOut(newCat);
       
       
       
       
       Dog sc = new Dog("Scooby", 5, true, false);
       // System.out.println(sc.wild);
       
       
       
    }
    
}
